# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/deshpande/Workspace/C++/osr_egs/src/3dor

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/deshpande/Workspace/C++/osr_egs/src/3dor/build

# Include any dependencies generated for this target.
include CMakeFiles/cg_recognition.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/cg_recognition.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/cg_recognition.dir/flags.make

CMakeFiles/cg_recognition.dir/cg_recognition.cpp.o: CMakeFiles/cg_recognition.dir/flags.make
CMakeFiles/cg_recognition.dir/cg_recognition.cpp.o: ../cg_recognition.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/deshpande/Workspace/C++/osr_egs/src/3dor/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/cg_recognition.dir/cg_recognition.cpp.o"
	/usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/cg_recognition.dir/cg_recognition.cpp.o -c /home/deshpande/Workspace/C++/osr_egs/src/3dor/cg_recognition.cpp

CMakeFiles/cg_recognition.dir/cg_recognition.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/cg_recognition.dir/cg_recognition.cpp.i"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/deshpande/Workspace/C++/osr_egs/src/3dor/cg_recognition.cpp > CMakeFiles/cg_recognition.dir/cg_recognition.cpp.i

CMakeFiles/cg_recognition.dir/cg_recognition.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/cg_recognition.dir/cg_recognition.cpp.s"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/deshpande/Workspace/C++/osr_egs/src/3dor/cg_recognition.cpp -o CMakeFiles/cg_recognition.dir/cg_recognition.cpp.s

CMakeFiles/cg_recognition.dir/cg_recognition.cpp.o.requires:
.PHONY : CMakeFiles/cg_recognition.dir/cg_recognition.cpp.o.requires

CMakeFiles/cg_recognition.dir/cg_recognition.cpp.o.provides: CMakeFiles/cg_recognition.dir/cg_recognition.cpp.o.requires
	$(MAKE) -f CMakeFiles/cg_recognition.dir/build.make CMakeFiles/cg_recognition.dir/cg_recognition.cpp.o.provides.build
.PHONY : CMakeFiles/cg_recognition.dir/cg_recognition.cpp.o.provides

CMakeFiles/cg_recognition.dir/cg_recognition.cpp.o.provides.build: CMakeFiles/cg_recognition.dir/cg_recognition.cpp.o

# Object files for target cg_recognition
cg_recognition_OBJECTS = \
"CMakeFiles/cg_recognition.dir/cg_recognition.cpp.o"

# External object files for target cg_recognition
cg_recognition_EXTERNAL_OBJECTS =

cg_recognition: CMakeFiles/cg_recognition.dir/cg_recognition.cpp.o
cg_recognition: CMakeFiles/cg_recognition.dir/build.make
cg_recognition: /usr/lib/x86_64-linux-gnu/libboost_system.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libboost_filesystem.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libboost_thread.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libboost_date_time.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libboost_iostreams.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libboost_serialization.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libboost_chrono.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libpthread.so
cg_recognition: /usr/lib/libpcl_common.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libflann_cpp_s.a
cg_recognition: /usr/lib/libpcl_kdtree.so
cg_recognition: /usr/lib/libpcl_octree.so
cg_recognition: /usr/lib/libpcl_search.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libqhull.so
cg_recognition: /usr/lib/libpcl_surface.so
cg_recognition: /usr/lib/libpcl_sample_consensus.so
cg_recognition: /usr/lib/libOpenNI.so
cg_recognition: /usr/lib/libOpenNI2.so
cg_recognition: /usr/lib/libvtkCommon.so.5.8.0
cg_recognition: /usr/lib/libvtkFiltering.so.5.8.0
cg_recognition: /usr/lib/libvtkImaging.so.5.8.0
cg_recognition: /usr/lib/libvtkGraphics.so.5.8.0
cg_recognition: /usr/lib/libvtkGenericFiltering.so.5.8.0
cg_recognition: /usr/lib/libvtkIO.so.5.8.0
cg_recognition: /usr/lib/libvtkRendering.so.5.8.0
cg_recognition: /usr/lib/libvtkVolumeRendering.so.5.8.0
cg_recognition: /usr/lib/libvtkHybrid.so.5.8.0
cg_recognition: /usr/lib/libvtkWidgets.so.5.8.0
cg_recognition: /usr/lib/libvtkParallel.so.5.8.0
cg_recognition: /usr/lib/libvtkInfovis.so.5.8.0
cg_recognition: /usr/lib/libvtkGeovis.so.5.8.0
cg_recognition: /usr/lib/libvtkViews.so.5.8.0
cg_recognition: /usr/lib/libvtkCharts.so.5.8.0
cg_recognition: /usr/lib/libpcl_io.so
cg_recognition: /usr/lib/libpcl_filters.so
cg_recognition: /usr/lib/libpcl_features.so
cg_recognition: /usr/lib/libpcl_keypoints.so
cg_recognition: /usr/lib/libpcl_registration.so
cg_recognition: /usr/lib/libpcl_segmentation.so
cg_recognition: /usr/lib/libpcl_recognition.so
cg_recognition: /usr/lib/libpcl_visualization.so
cg_recognition: /usr/lib/libpcl_people.so
cg_recognition: /usr/lib/libpcl_outofcore.so
cg_recognition: /usr/lib/libpcl_tracking.so
cg_recognition: /usr/lib/libpcl_apps.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libboost_system.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libboost_filesystem.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libboost_thread.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libboost_date_time.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libboost_iostreams.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libboost_serialization.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libboost_chrono.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libpthread.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libqhull.so
cg_recognition: /usr/lib/libOpenNI.so
cg_recognition: /usr/lib/libOpenNI2.so
cg_recognition: /usr/lib/x86_64-linux-gnu/libflann_cpp_s.a
cg_recognition: /usr/lib/libvtkCommon.so.5.8.0
cg_recognition: /usr/lib/libvtkFiltering.so.5.8.0
cg_recognition: /usr/lib/libvtkImaging.so.5.8.0
cg_recognition: /usr/lib/libvtkGraphics.so.5.8.0
cg_recognition: /usr/lib/libvtkGenericFiltering.so.5.8.0
cg_recognition: /usr/lib/libvtkIO.so.5.8.0
cg_recognition: /usr/lib/libvtkRendering.so.5.8.0
cg_recognition: /usr/lib/libvtkVolumeRendering.so.5.8.0
cg_recognition: /usr/lib/libvtkHybrid.so.5.8.0
cg_recognition: /usr/lib/libvtkWidgets.so.5.8.0
cg_recognition: /usr/lib/libvtkParallel.so.5.8.0
cg_recognition: /usr/lib/libvtkInfovis.so.5.8.0
cg_recognition: /usr/lib/libvtkGeovis.so.5.8.0
cg_recognition: /usr/lib/libvtkViews.so.5.8.0
cg_recognition: /usr/lib/libvtkCharts.so.5.8.0
cg_recognition: /usr/lib/libpcl_common.so
cg_recognition: /usr/lib/libpcl_kdtree.so
cg_recognition: /usr/lib/libpcl_octree.so
cg_recognition: /usr/lib/libpcl_search.so
cg_recognition: /usr/lib/libpcl_surface.so
cg_recognition: /usr/lib/libpcl_sample_consensus.so
cg_recognition: /usr/lib/libpcl_io.so
cg_recognition: /usr/lib/libpcl_filters.so
cg_recognition: /usr/lib/libpcl_features.so
cg_recognition: /usr/lib/libpcl_keypoints.so
cg_recognition: /usr/lib/libpcl_registration.so
cg_recognition: /usr/lib/libpcl_segmentation.so
cg_recognition: /usr/lib/libpcl_recognition.so
cg_recognition: /usr/lib/libpcl_visualization.so
cg_recognition: /usr/lib/libpcl_people.so
cg_recognition: /usr/lib/libpcl_outofcore.so
cg_recognition: /usr/lib/libpcl_tracking.so
cg_recognition: /usr/lib/libpcl_apps.so
cg_recognition: /usr/lib/libvtkViews.so.5.8.0
cg_recognition: /usr/lib/libvtkInfovis.so.5.8.0
cg_recognition: /usr/lib/libvtkWidgets.so.5.8.0
cg_recognition: /usr/lib/libvtkVolumeRendering.so.5.8.0
cg_recognition: /usr/lib/libvtkHybrid.so.5.8.0
cg_recognition: /usr/lib/libvtkParallel.so.5.8.0
cg_recognition: /usr/lib/libvtkRendering.so.5.8.0
cg_recognition: /usr/lib/libvtkImaging.so.5.8.0
cg_recognition: /usr/lib/libvtkGraphics.so.5.8.0
cg_recognition: /usr/lib/libvtkIO.so.5.8.0
cg_recognition: /usr/lib/libvtkFiltering.so.5.8.0
cg_recognition: /usr/lib/libvtkCommon.so.5.8.0
cg_recognition: /usr/lib/libvtksys.so.5.8.0
cg_recognition: CMakeFiles/cg_recognition.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable cg_recognition"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/cg_recognition.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/cg_recognition.dir/build: cg_recognition
.PHONY : CMakeFiles/cg_recognition.dir/build

CMakeFiles/cg_recognition.dir/requires: CMakeFiles/cg_recognition.dir/cg_recognition.cpp.o.requires
.PHONY : CMakeFiles/cg_recognition.dir/requires

CMakeFiles/cg_recognition.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/cg_recognition.dir/cmake_clean.cmake
.PHONY : CMakeFiles/cg_recognition.dir/clean

CMakeFiles/cg_recognition.dir/depend:
	cd /home/deshpande/Workspace/C++/osr_egs/src/3dor/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/deshpande/Workspace/C++/osr_egs/src/3dor /home/deshpande/Workspace/C++/osr_egs/src/3dor /home/deshpande/Workspace/C++/osr_egs/src/3dor/build /home/deshpande/Workspace/C++/osr_egs/src/3dor/build /home/deshpande/Workspace/C++/osr_egs/src/3dor/build/CMakeFiles/cg_recognition.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/cg_recognition.dir/depend

