# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/deshpande/Workspace/C++/osr_egs/src/modules/PCDViz.cpp" "/home/deshpande/Workspace/C++/osr_egs/src/modules/build/CMakeFiles/pcdviz.dir/PCDViz.cpp.o"
  "/home/deshpande/Workspace/C++/osr_egs/src/modules/main.cpp" "/home/deshpande/Workspace/C++/osr_egs/src/modules/build/CMakeFiles/pcdviz.dir/main.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "DISABLE_PCAP"
  "DISABLE_PNG"
  "EIGEN_USE_NEW_STDVECTOR"
  "EIGEN_YES_I_KNOW_SPARSE_MODULE_IS_NOT_STABLE_YET"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/include/vtk-5.8"
  "/usr/include/pcl-1.7"
  "/usr/local/include/eigen3"
  "/usr/include/ni"
  "/usr/include/openni2"
  "../../modules"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
