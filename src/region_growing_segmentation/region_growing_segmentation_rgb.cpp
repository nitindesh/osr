/*
Author : Nitin Deshpande (Region Growing), Saad Abdullah (Visualisation PCD), Radiah Rivu (Visualisation PLY)
Code segment to demonstrate Region Growing RGB segmentation on PCDs
requires PCL 1.7
*/
#include <iostream>
#include <vector>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/region_growing_rgb.h>

int main (int argc, char** argv)
{
  pcl::search::Search <pcl::PointXYZRGB>::Ptr pcl_tree = boost::shared_ptr<pcl::search::Search<pcl::PointXYZRGB> > (new pcl::search::KdTree<pcl::PointXYZRGB>);

  pcl::PointCloud <pcl::PointXYZRGB>::Ptr pt_cloud (new pcl::PointCloud <pcl::PointXYZRGB>);
  if ( pcl::io::loadPCDFile <pcl::PointXYZRGB> ("test44.pcd", *pt_cloud) == -1 )
  {
    std::cout << "Could not read the PC data." << std::endl;
    return (-1);
  }

  pcl::IndicesPtr pc_indices (new std::vector <int>);
  pcl::PassThrough<pcl::PointXYZRGB> pass;
  pass.setInputCloud (pt_cloud);
  pass.setFilterFieldName ("z");
  pass.setFilterLimits (0.0, 1.0);
  pass.filter (*pc_indices);

  pcl::RegionGrowingRGB<pcl::PointXYZRGB> reg_growing_seg;
  reg_growing_seg.setInputCloud (pt_cloud);
  reg_growing_seg.setIndices (pc_indices);
  reg_growing_seg.setSearchMethod (pcl_tree);
  reg_growing_seg.setDistanceThreshold (10);

  /*
  check whether the point is neighbouring or not.
  if (dist(point) < threshold)
  	neighbor
  	clustersearhc()
  else
  	not_a_neighbor
  */
  reg_growing_seg.setPointColorThreshold (6);
  reg_growing_seg.setRegionColorThreshold (5);
  // merge with the nearest neighbors
  reg_growing_seg.setMinClusterSize (600);

  // initialise clusters
  std::vector <pcl::PointIndices> pt_clusters;
  // when the  segmentation is finished return the array of clusters
  /*TODO each cluster should have its own color*/
  reg_growing_seg.extract (pt_clusters);

  pcl::PointCloud <pcl::PointXYZRGB>::Ptr colored_cloud = reg_growing_seg.getColoredCloud ();
  // region growing visualisation
  /*TODO: Visualisation class*/
  pcl::visualization::CloudViewer cloud_viewer ("RGB Region Growing");
  // display cloud
  cloud_viewer.showCloud (colored_cloud);
  while (!cloud_viewer.wasStopped ())
  {
	// use boost to send
    boost::this_thread::sleep (boost::posix_time::microseconds (100));
  }

  return (0);
}
