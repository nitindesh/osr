cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(reg_grow_seg)

find_package(PCL 1.5 REQUIRED)

include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

add_executable (reg_grow_seg reg_grow_seg.cpp)
target_link_libraries (reg_grow_seg ${PCL_LIBRARIES})
