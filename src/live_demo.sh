cd modules/build
./cloud_viz cerealbox0.pcd cerealbox1.pcd

cd ../..
cd 3dor/build/
./cg_recognition cerealbox0.pcd cerealbox1.pcd

echo "Live Demo Region Growing RGB"
sleep 2

cd ../..
cd modules/build
./cloud_viz test44.pcd

cd ../..
cd region_growing_segmentation/build
./reg_grow_seg
cd ../..
echo "Live Demo Region Growing Monochrome"
sleep 2

cd modules/build
./cloud_viz 1466946023683136.pcd

cd ../..
cd region_growing_monochrome_segmentation/build
./region_growing_segmentation
